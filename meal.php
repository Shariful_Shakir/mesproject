<?php include("db_connection.php")?>

<?php
	$sql = "SELECT id, name FROM person";
	$result = mysqli_query($conn, $sql);
	if(isset($_POST['submit'])){
		$person_id = $_POST['person_name'];
		$lunchNumber = $_POST['lunchNumber'];
		$dinnerNumber = $_POST['dinnerNumber'];
		$daily_total_meal = (int)$lunchNumber + (int)$dinnerNumber;

		$total_meal = "SELECT MAX(total_meal) AS 't_meal' FROM meal WHERE person_id = $person_id";
		$total_meal = mysqli_query($conn, $total_meal);
		//print_r($total_meal);

		if (mysqli_num_rows($total_meal) > 0) {
	   
		    while($row = mysqli_fetch_assoc($total_meal)) { 
			$total_meal_count = $row['t_meal'];
		    }
		}
		$total_meal = (int)$daily_total_meal + (int)$total_meal_count;

		$date = $_POST['date'];
		//echo $person_id.$lunchNumber.$dinnerNumber.$daily_total_meal.$total_meal;die;
		$sql = "INSERT INTO meal(person_id, lunch, dinner, date, daily_total_meal, total_meal)VALUES ('$person_id', '$lunchNumber', '$dinnerNumber', '$date', $daily_total_meal, $total_meal
)";

		if (mysqli_query($conn, $sql)) {
		    echo "<script>alert('Success')</script>";
		} else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	}
?>

<?php include("navbar.php") ?>

  <h2>কত বেলা খাবেন ?</h2>
<form action="" method="POST">
<div class="form-group">
    <label for="name">ব্যাক্তি নির্বাচন:</label>
   <select class="form-control" name="person_name" required>
	<option value="">--নির্বাচন-- </option>
	<?php 
	if (mysqli_num_rows($result) > 0) {
	    // output data of each row
	    while($row = mysqli_fetch_assoc($result)) { ?>
		<option value="<?php echo $row["id"]; ?>"> <?php echo $row["name"]; ?> </option>
	 <?php   }
	}
	?>
  	
   </select>
 </div>

<div class="form-group">
    <label for="name">তারিখ :</label>
    <input type="date" class="form-control" id="date" name="date" required >
  </div>
  <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="lunch" value="option1">
  <label class="form-check-label" for="lunch">দুপুর </label><span id='addLunch'></span>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="dinner" value="option2">
  <label class="form-check-label" for="lunch">রাত </label><span id='addDinner'></span>
</div>
<br><br>
<button type="submit" name="submit" class="btn btn-primary">সাবমিট</button>
</form>
</div>

</body>
</html>
<script>
	$(document).ready(function(){
		$('#lunch').on('click', function(){
		    if($(this).is(':checked')) {
			$('#addLunch').html('&nbsp;&nbsp;&nbsp;<input type="number" name="lunchNumber" id="lunchNumber" placeholder="খাবারের সংখ্যা" required>');
		    } else {
			$('#lunchNumber').remove();
		    }		
		});

		$('#dinner').on('click', function(){
		    if($(this).is(':checked')) {
			$('#addDinner').html('&nbsp;&nbsp;&nbsp;<input type="number" name="dinnerNumber" id="dinnerNumber" placeholder="খাবারের সংখ্যা " required>');
		    } else {
			$('#dinnerNumber').remove();
		    }		
		});
	});
</script>

