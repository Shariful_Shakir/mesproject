<?php include("db_connection.php")?>

<?php
	$sql = "SELECT  person.id,person.name,meal.person_id,meal.dinner,meal.lunch,meal.date,meal.daily_total_meal,meal.total_meal FROM person, meal WHERE person.id = meal.person_id ORDER BY meal.date DESC";
	$result = mysqli_query($conn, $sql);
	//print_r($result);die;
?>
	
	<?php 
	$total_taka = 0;
	$sql_total_taka = "SELECT  SUM(taka) AS total_taka FROM bajar";
	$result_total_taka = mysqli_query($conn, $sql_total_taka);
	if (mysqli_num_rows($result_total_taka) > 0) {
	    while($row = mysqli_fetch_assoc($result_total_taka)) { 
		$total_taka += $row['total_taka'];
	  }
	}//echo $total_taka;
	
	$total_meal = 0;
	$sql_total_meal = "SELECT SUM(daily_total_meal) AS total_meal FROM meal";
	$result_total_meal = mysqli_query($conn, $sql_total_meal);
	if (mysqli_num_rows($result_total_meal) > 0) {
	    while($row = mysqli_fetch_assoc($result_total_meal)) { 
		$total_meal += $row['total_meal'];
	  }
	}//echo $total_meal;

	$meal_rate = $total_taka/$total_meal;
	//echo $meal_rate;
	?>



<?php include("navbar.php") ?>

  <h2></h2>
  <p></p>
  <span style="background-color: green; color:white;">মোট মিল: <?php echo $total_meal; ?></span>
  <span style="background-color: blue; color:white;">মোট বাজার: <?php echo $total_taka; ?></span>          
  <span style="background-color: pink;">মিল রেট: <?php echo sprintf('%.3lf',$meal_rate); ?></span>          
  <table class="table">
    <thead>
      <tr>
	<th>তারিখ</th>
        <th>নাম</th>
        
        <th>দুপুর</th>
	<th>রাত</th>
	<th>আজ</th>
	<th>সবমিলে</th> 
	<th>খরচ (টাকা)</th> 
	<th>টাকা আছে</th> 
      </tr>
    </thead>
    <tbody>
	<?php 
	if (mysqli_num_rows($result) > 0) {
	    while($row = mysqli_fetch_assoc($result)) { //print_r($row);?>
		
		<tr>
		<td><?php echo $row["date"]; ?></td>
		<td><?php echo $row["name"]; ?></td>
		<td><?php echo $row["lunch"]; ?></td>
		<td><?php echo $row["dinner"]; ?></td>
		<td><?php echo $row["daily_total_meal"]; ?></td>
		<td><?php echo $row["total_meal"]; ?></td>
		<td><?php echo $spand_balance = sprintf('%.3lf',$meal_rate * (float)$row["total_meal"]); ?></td>
		<td><?php //echo $row["taka"]; ?></td>
      		</tr>
		
	 <?php   }
	}
	?>
     
    </tbody>
  </table>

	
</div>

</body>
</html>

