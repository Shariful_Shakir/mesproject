-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 30, 2019 at 04:03 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mesProject`
--

-- --------------------------------------------------------

--
-- Table structure for table `bajar`
--

CREATE TABLE `bajar` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `taka` double NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bajar`
--

INSERT INTO `bajar` (`id`, `person_id`, `taka`, `date`) VALUES
(1, 1, 520, '2019-06-29'),
(2, 2, 400, '2019-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `meal`
--

CREATE TABLE `meal` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `lunch` int(11) DEFAULT NULL,
  `dinner` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `daily_total_meal` int(11) DEFAULT NULL,
  `total_meal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meal`
--

INSERT INTO `meal` (`id`, `person_id`, `lunch`, `dinner`, `date`, `daily_total_meal`, `total_meal`) VALUES
(1, 1, 1, 2, '2019-06-29', 3, 3),
(3, 1, 1, 1, '2019-06-30', 2, 5),
(4, 1, 2, 2, '2019-07-01', 4, 9),
(5, 2, 1, 3, '2019-06-29', 4, 4),
(6, 2, 2, 1, '2019-06-30', 3, 7),
(7, 3, 3, 1, '2019-06-30', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `bajar_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `contact` int(11) DEFAULT NULL,
  `person_total_meal` int(11) DEFAULT NULL,
  `person_total_taka` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `bajar_id`, `name`, `contact`, `person_total_meal`, `person_total_taka`) VALUES
(1, NULL, 'Shariful Islam', 1773592770, NULL, NULL),
(2, NULL, 'Joynal', 1745260495, NULL, NULL),
(3, NULL, 'Rayhan', 1737698971, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `person_joma_taka`
--

CREATE TABLE `person_joma_taka` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `taka` double NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person_joma_taka`
--

INSERT INTO `person_joma_taka` (`id`, `person_id`, `taka`, `date`) VALUES
(1, 1, 500, '2019-06-29'),
(2, 3, 1000, '2019-06-29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bajar`
--
ALTER TABLE `bajar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meal`
--
ALTER TABLE `meal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_joma_taka`
--
ALTER TABLE `person_joma_taka`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bajar`
--
ALTER TABLE `bajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meal`
--
ALTER TABLE `meal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `person_joma_taka`
--
ALTER TABLE `person_joma_taka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
