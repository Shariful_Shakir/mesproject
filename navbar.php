<!DOCTYPE html>
<html lang="en">
<head>
  <title>MesProject</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
<nav class="navbar navbar-expand-sm bg-light">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="index.php">হোম </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="person_reg.php">রেজিস্ট্রেশন </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="person_taka_joma.php">টাকা জমা </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="meal.php">খাবার</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="bajar.php">বাজার</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="bajar_khoroch.php">খরচ</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="person_personal_report.php">ব্যক্তিগত রিপোর্ট</a>
    </li>
  </ul>
</nav>
<br>
